package springcloud.service.ribbon.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Raye on 2017/5/22.
 */
@RestController
@RequestMapping(value = "ribbon")
public class Controller {

    @Resource
    private RestTemplate template;
    @RequestMapping("hello")
    @HystrixCommand(fallbackMethod = "helloError")
    public String ribbon(String name){
        return template.getForObject("http://service-feign/feign/ribbonhello?name="+name,String.class);
    }

    @RequestMapping("feignhello")
    public String hello(String name){
        return "hello "+name+" this is ribbon spring cloud";
    }
    public String helloError(String name){
        return "feign服务挂了";
    }
}