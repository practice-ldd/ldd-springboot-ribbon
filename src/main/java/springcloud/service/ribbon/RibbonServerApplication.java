package springcloud.service.ribbon;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard
//@ComponentScan(basePackages = {"com.demo2do","com.suidifu", "com.zufangbao"})
public class RibbonServerApplication {

    public static void main(String[] args) {

        SpringApplication.run(RibbonServerApplication.class, args);

    }
}
